<?php

namespace App\Http\Controllers\frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Project;
use App\Vacancy;
use App\Visit;
use Carbon\Carbon;

class PageController extends Controller
{
    public function app(Request $request) {

        $offerExpire = '';
        $offerExtensionDay = 25;
        $today = Carbon::now();

        //date change after offerExtensionDay
        if($today->day >= $offerExtensionDay) {
            $lastDay = $today->modify('1 month')->endOfMonth()->day;
        } else {
            $lastDay = $today->endOfMonth()->day;
        };
        $monthLocal = '';
        //next month code
        switch($today->format('M')) {
            case 'Jan': $monthLocal = 'января'; break;
            case 'Feb': $monthLocal = 'февраля'; break;
            case 'Mar': $monthLocal = 'марта'; break;
            case 'Apr': $monthLocal = 'апреля'; break;
            case 'May': $monthLocal = 'мая'; break;
            case 'Jun': $monthLocal = 'июня'; break;
            case 'Jul': $monthLocal = 'июля'; break;
            case 'Aug': $monthLocal = 'августа'; break;
            case 'Sep': $monthLocal = 'сентября'; break;
            case 'Oct': $monthLocal = 'октября'; break;
            case 'Nov': $monthLocal = 'ноября'; break;
            case 'Dec': $monthLocal = 'декабря'; break;
            default: break;
        }
        $offerExpire = $lastDay.' '.$monthLocal;
        // dd($offerExpire);

        return view('layouts.site',['offerExpire' => $offerExpire]);
    }

    public function quizSite(Request $request) {

        return view('layouts.quiz_site');
    }

    public function quizPromo(Request $request) {

        return view('layouts.quiz_promo');
    }

    public function quizPromo2(Request $request) {

        return view('layouts.quiz_promo2');
    }

    public function spa(Request $request) {

        return [
            'projects' => Project::where('status', 'active')->get(),
            'vacancys' => Vacancy::where('status', 'active')->get()
        ];
    }

    public function lead(){
        return view('layouts.lead');
    }

}
