import Vue from 'vue';
window.Vue = Vue;
import store from './quiz2store';

Vue.component('App', require('./views/QuizPromo2.vue').default);

new Vue({
    el: '#app',
    store
})